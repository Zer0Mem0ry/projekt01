import requests
import json
import tempfile
import cv2
from bs4 import BeautifulSoup

url = requests.get("https://www.junkers.pl/produkty/przygotowanie-cwu/gazowe-podgrzewacze-przeplywowe/w-11-kb.html")
x = url.text
soup = BeautifulSoup(x, 'html.parser')
body = soup.body

class Dane():
    def title(self):
        print("Nazwa: " + soup.title.string)

    def k_opis(self):
        div = body.find('div', {'id': 'tabs'})
        p = str(div.p.extract())
        opis = str(json.dumps(p))
        print("Krótki opis: " + opis)

    def d_opis(self):
        div2 = body.find('ul', {'class': 'simple'})
        ul = str(div2.extract())
        dopis = str(json.dumps(ul))
        print("Dlugi opis: " + dopis)

    def d_image(self):
        r = requests.get('https://www.junkers.pl/images/IMG_9978_lay_PL.jpg')
        with open('image.png', 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if not chunk:
                    break
                f.write(chunk)

    def d_tech(self):
        rows = iter(soup.find('tbody').find_all('tr'))
        #Nie mialem pomyslu jak to zrobic inaczej, poniewaz nie moglem znalezc zadnego odnosnika do samych danych technicznych w zrodle html.
        next(rows)
        next(rows)
        next(rows)
        next(rows)
        next(rows)
        next(rows)
        next(rows)
        next(rows)
        next(rows)
        next(rows)
        print("Dane techniczne: ")
        for row in rows:
            colum = row.find_all('td')
            colum = [x.text.strip() for x in colum]
            print(colum)

    def t_file(self):
        with tempfile.TemporaryDirectory() as td:
            print("Lokalizacja folderu: ", td)

    def kadr(self):
        image = cv2.imread("image.png")
        cv2.namedWindow("Contour", cv2.WINDOW_AUTOSIZE)
        imgray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(imgray, 127, 255, 0)
        im2, contours, hierarhy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        for i in range(0, len(contours)):
            x, y, w, h = cv2.boundingRect(contours[i])
        result = imgray[y:y + h, x:x + w]
        cv2.imwrite("contour" + str(i) + ".png", result)

dane = Dane()

dane.title()
dane.t_file()
dane.k_opis()
dane.d_opis()
dane.d_image()
dane.d_tech()
dane.kadr()